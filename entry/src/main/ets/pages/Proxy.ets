/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { HttpClient, Request, Response, TimeUnit ,Proxy, Type,Dns} from '@ohos/httpclient'
import connection from '@ohos.net.connection'
import { BusinessError } from '@ohos/httpclient/src/main/ets/http';
import { CustomEmptyDns } from './dns';

@Entry
@Component
struct ProxyDemo {

  @State info:string = ''
  @State msg: string = "";
  @State status: string = "";
  @State content: string = "";
  ipInput: string = '1.94.37.200'
  portInput: number = 6443
  @State urlPath:string = 'http://www.baidu.com'

  aboutToAppear(): void {

  }

  build() {
    Column(){
      Flex({ direction: FlexDirection.Column }) {
        Navigator({ target: "", type: NavigationType.Back }) {
          Text('BACK')
            .fontSize(12)
            .border({ width: 1 })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Text('代理服务器ip')
        .fontSize(14)
        .padding(10)
        .width('90%')
        .fontColor(0x000000)

      TextInput({ text: this.ipInput, placeholder: '输入服务器ip' })
        .width('90%')
        .height('40')
        .fontSize(18)
        .onChange((value: string) => {
          this.ipInput = value
        })
      Text('代理服务器端口')
        .fontSize(14)
        .padding(10)
        .width('90%')
        .fontColor(0x000000)

      TextInput({ text: this.portInput.toString(), placeholder: '输入服务器端口' })
        .width('90%')
        .height('40')
        .fontSize(18)
        .onChange((value: string) => {
          this.ipInput = value
        })

      Text('请求网址')
        .fontSize(14)
        .padding(10)
        .width('90%')
        .fontColor(0x000000)

      TextInput({ text: this.urlPath, placeholder: '输入服务器端口' })
        .width('90%')
        .height('40')
        .fontSize(18)
        .onChange((value: string) => {
          this.urlPath = value
        })

      Button('请求网络')
        .width('80%')
        .height('40')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          this.setProxy()
        })

      Text(this.info)
        .fontSize(18)
        .width('85%')
        .height('25%')
        .border({ width: 2, color: Color.Black })
        .margin(10)
    }
    .width('100%')
    .height('100%')
  }

  setProxy() {
    //创建代理
    let client: HttpClient = new HttpClient
      .Builder()
      .setProxy(new Proxy(Type.HTTP,'1.94.37.200',6443))
      .dns(new CustomDns())
      .setConnectTimeout(10, TimeUnit.SECONDS)
      .setReadTimeout(10, TimeUnit.SECONDS)
      .build();
    let request: Request = new Request.Builder()
      .url('http://www.baidu.com')
      .method('GET')
      .build();
    client.newCall(request).enqueue((result: Response) => {
      this.info = "返回值值："+result.responseCode.toString()
    }, (err: ESObject) => {
      this.info = JSON.stringify(err)
      console.info('err ========' + JSON.stringify(err))
    });
  }
}

export class CustomDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        //解析出来的网址|1是Ipv4，2是IPV6|端口号
        resolve(netAddress)
      }).catch((err: BusinessError) => {
        reject(err)
      });
    })
  }
}