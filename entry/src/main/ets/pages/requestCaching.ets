/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import socket from '@ohos.net.socket';
import {
  Cache,
  CacheControl,
  Dns,
  HttpClient,
  Logger,
  Request,
  Response,
  StringUtil,
  TimeUnit,
  Utils,
  X509TrustManager
} from '@ohos/httpclient';
import { Utils as GetCAUtils } from "../utils/Utils";
import certFramework from '@ohos.security.cert';
import base64 from 'base64-js';
import connection from '@ohos.net.connection';

const TAG: string = "request_caching_customCertificate_Unidirectional";

@Entry
@Component
struct requestCaching {
  @State timeOut: number = -1;
  @State headerFlag: number = 0;
  @State headerName: string = 'https';
  @State result: string = '';
  @State status: string = '';
  @State content: string = '';
  @State data: string = '';
  @State apiInput: string = '';
  ipInput: string = '1.94.37.200'
  portInput: string = '8080'
  caPem = "noPassword/ca.crt";

  build() {
    Column() {
      Flex({
        direction: FlexDirection.Row
      }) {
        Navigator({
          target: "",
          type: NavigationType.Back
        }) {
          Text('BACK')
            .fontSize(12)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
        Row() {
          Text('https')
            .fontSize(20)
            .padding(10)
            .margin({ right: 20 })
            .fontColor(this.headerFlag === 0 ? Color.Green : Color.Gray)
            .onClick(() => {
              this.headerFlag = 0;
              this.portInput = '8080';
              this.headerName = 'https';
            })
          Text('http')
            .padding(10)
            .fontSize(20)
            .fontColor(this.headerFlag === 0 ? Color.Gray : Color.Green)
            .onClick(() => {
              this.headerFlag = 1;
              this.portInput = '7070';
              this.headerName = 'http';
            })
        }
        .justifyContent(FlexAlign.End)
        .flexGrow(1)
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Flex({
        direction: FlexDirection.Column,
        alignItems: ItemAlign.Center
      }) {
        TextInput({ text: this.ipInput, placeholder: '输入服务器ip' })
          .width('80%')
          .height('100%')
          .fontSize(18)
          .margin({ top: px2vp(20) })
          .height(px2vp(150))
          .enabled(false)
          .onChange((value: string) => {
            this.ipInput = value
          })

        TextInput({ text: this.headerFlag === 0 ? '8080' : '7070', placeholder: '输入服务器端口' })
          .width('80%')
          .height('100%')
          .fontSize(18)
          .margin({ top: px2vp(20) })
          .height(px2vp(150))
          .enabled(false)
          .onChange((value: string) => {
            this.portInput = value
          })

        TextInput({ text: this.apiInput, placeholder: '输入缓存接口' })
          .width('80%')
          .height('100%')
          .fontSize(18)
          .margin({ top: px2vp(20) })
          .height(px2vp(150))
          .enabled(false)
          .onChange((value: string) => {
            this.apiInput = value
          })

        Column() {
          Text('查询结果来源：' + this.result)
            .fontSize(18)
            .width('100%')
            .textAlign(TextAlign.Center)
            .margin({ top: 10, bottom: 10 })
          Text('返回码：' + this.status)
            .fontSize(18)
            .width('100%')
            .textAlign(TextAlign.Center)
            .margin({ bottom: 10 })
          Text(this.content)
            .fontSize(18)
            .width('100%')
            .textAlign(TextAlign.Center)
        }
        .height('30%')
        .width('95%')
        .margin({ top: 10 })
        .borderWidth(1)

        Row() {
          Button('e/tag')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin({ right: 20 })
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/e/tag';
              this.content = "waiting for response";
              this.request(0);
            })

          Button('e/tag/change')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/e/tag/change'
              this.content = "waiting for response";
              this.request(1);
            })
        }
        .width('100%')
        .margin({ bottom: 10, top: 10 })
        .justifyContent(FlexAlign.Center)

        Row() {
          Button('expires')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin({ right: 20 })
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/expires'
              this.content = "waiting for response";
              this.request(2);
            })

          Button('immutable')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/immutable'
              this.content = "waiting for response";
              this.request(3);
            })
        }
        .width('100%')
        .margin({ bottom: 10 })
        .justifyContent(FlexAlign.Center)

        Row() {
          Button('last/modified')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin({ right: 20 })
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/last/modified'
              this.content = "waiting for response";
              this.request(4);
            })
          Button('modify/change')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/last/modified/change'
              this.content = "waiting for response";
              this.request(5);
            })
        }
        .width('100%')
        .margin({ bottom: 10 })
        .justifyContent(FlexAlign.Center)

        Row() {
          Button('max/age')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin({ right: 20 })
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/max/age'
              this.content = "waiting for response";
              this.request(6);
            })

          Button('no/cache')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/no/cache'
              this.content = "waiting for response";
              this.request(7);
            })
        }
        .width('100%')
        .margin({ bottom: 10 })
        .justifyContent(FlexAlign.Center)

        Row() {
          Button('forceNetwork')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin({ right: 20 })
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/max/age'
              this.content = "waiting for response";
              this.request(8);
            })

          Button('forceCache')
            .width('40%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .onClick(async (event: ClickEvent) => {
              this.apiInput = '/cache/max/age'
              this.content = "waiting for response";
              this.request(9);
            })
        }
        .width('100%')
        .justifyContent(FlexAlign.Center)

      }
      .margin({ top: 5 })
      .height('100%')
      .width('100%')
    }
    .alignItems(HorizontalAlign.Center)
  }

  async request(flag: number) {
    let context = getContext();
    let hereCacheDir: string = context.cacheDir;
    let cache: Cache.Cache = new Cache.Cache(hereCacheDir, 10 * 1024 * 1024, context);
    let httpClient: HttpClient = new HttpClient
      .Builder()
      .dns((this.headerFlag === 0) ? new CustomDnsForHttps() : new CustomDnsForHttp())
      .cache(cache)
      .setConnectTimeout(10000, TimeUnit.SECONDS)
      .setReadTimeout(10000, TimeUnit.SECONDS)
      .build();

    let caFile: string = await new GetCAUtils().getCA(this.caPem, context);
    let url: string = this.headerName + '://' + this.ipInput + ':' + this.portInput + this.apiInput;
    let request: Request = {} as Request;
    if (flag <= 5) {
      request = new Request.Builder()
        .get()
        .url(url)
        .ca(this.headerFlag === 0 ? [caFile] : '')
        .build();
    } else if (flag === 6) {
      request = new Request.Builder()
        .get()
        .url(url)
        .addHeader("Cache-Control", "max-age=30")
        .ca(this.headerFlag === 0 ? [caFile] : '')
        .build();
    } else if (flag === 7) {
      request = new Request.Builder()
        .get()
        .url(url)
        .addHeader("Cache-Control", "no-cache")
        .ca(this.headerFlag === 0 ? [caFile] : '')
        .build();
    } else if (flag === 8) {
      request = new Request.Builder()
        .get()
        .url(url)
        .cacheControl(CacheControl.FORCE_NETWORK())
        .ca(this.headerFlag === 0 ? [caFile] : '')
        .build();
    }
    else if (flag === 9) {
      request = new Request.Builder()
        .get()
        .url(url)
        .cacheControl(CacheControl.FORCE_CACHE())
        .ca(this.headerFlag === 0 ? [caFile] : '')
        .build();
    }

    // 防抖（1s）请求
    clearTimeout(this.timeOut);
    this.timeOut = -1;
    this.timeOut = setTimeout(() => {
      httpClient
        .newCall(request)
        .checkCertificate(new SslCertificateManager())
        .execute()
        .then((result: Response) => {
          if (result.getNetWorkResponse()) {
            if (result.getNetWorkResponse().responseCode === 200) {
              this.result = '网络';
              this.status = result.responseCode + '';
              if (((flag === 1) || (flag === 5)) && (result.result === '"true"')) {
                this.content = '修改成功';
              } else {
                this.content = result.result;
              }
            }
            if (result.getNetWorkResponse().responseCode === 304) {
              this.result = '缓存';
              this.status = result.responseCode + '';
              this.content = result.result;
            }
          } else {
            this.result = '缓存';
            this.status = result.getCacheResponse().getCode() + '';
            this.content = result.getCacheResponse().getBody() + '';
          }
        }, (error: Response) => {
          this.status = '';
          this.result = '';
          this.content = JSON.stringify(error);
        });
    }, 1000);
  }
}

export class CustomDnsForHttps implements Dns {
  async lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    console.info('DNSTEST CustomDns begin here');
    return await new Promise((resolve, reject) => {
      let netAddress: Array<connection.NetAddress> = [{ 'address': '1.94.37.200', 'family': 1, 'port': 8080 }];
      resolve(netAddress)
    })
  }
}

export class CustomDnsForHttp implements Dns {
  async lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    console.info('DNSTEST CustomDns begin here');
    return await new Promise((resolve, reject) => {
      let netAddress: Array<connection.NetAddress> = [{ 'address': '1.94.37.200', 'family': 1, 'port': 7070 }];
      resolve(netAddress)
    })
  }
}

export class SslCertificateManager implements X509TrustManager {
  // 校验服务器证书
  checkServerTrusted(X509Certificate: certFramework.X509Cert): void {
    Logger.info(TAG, 'Get Server Trusted X509Certificate');
    // 时间校验成功的设置值
    let currentDayTime: number = StringUtil.getCurrentDayTime();
    let date = currentDayTime + 'Z';
    try {
      X509Certificate.checkValidityWithDate(date); // 检查X509证书有效期
      console.error('checkValidityWithDate success');
    } catch (error) {
      console.error('checkValidityWithDate failed, errCode: ' + error.code + ', errMsg: ' + error.message);
      error.message = 'checkValidityWithDate failed, errCode: ' + error.code + ', errMsg: ' + error.message;
      throw new Error(error);
    }
  }

  // 校验客户端证书
  checkClientTrusted(X509Certificate: certFramework.X509Cert): void {
    Logger.info(TAG, 'Get Client Trusted X509Certificate');
    let encoded = X509Certificate.getEncoded(); // 获取X509证书序列化数据
    Logger.info(TAG, 'encoded: ', JSON.stringify(encoded));
    let publicKey = X509Certificate.getPublicKey(); // 获取X509证书公钥
    Logger.info(TAG, 'publicKey: ', JSON.stringify(publicKey));
    let version = X509Certificate.getVersion(); // 获取X509证书版本
    Logger.info(TAG, 'version: ', JSON.stringify(version));
    let serialNumber = X509Certificate.getCertSerialNumber(); //获取X509证书序列号
    Logger.info(TAG, 'serialNumber: ', serialNumber);
    let issuerName = X509Certificate.getIssuerName(); // 获取X509证书颁发者名称
    Logger.info(TAG, 'issuerName: ', Utils.uint8ArrayToString(issuerName.data));
    let subjectName = X509Certificate.getSubjectName(); // 获取X509证书主体名称
    Logger.info(TAG, 'subjectName: ', Utils.uint8ArrayToString(subjectName.data));
    let notBeforeTime = X509Certificate.getNotBeforeTime(); // 获取X509证书有效期起始时间
    Logger.info(TAG, 'notBeforeTime: ', notBeforeTime);
    let notAfterTime = X509Certificate.getNotAfterTime(); // 获取X509证书有效期截止时间
    Logger.info(TAG, 'notAfterTime: ', notAfterTime);
    let signature = X509Certificate.getSignature(); // 获取X509证书签名数据
    Logger.info(TAG, 'signature: ', Utils.uint8ArrayToString(signature.data));
    let signatureAlgName = X509Certificate.getSignatureAlgName(); // 获取X509证书签名算法名称
    Logger.info(TAG, 'signatureAlgName: ', signatureAlgName);
    let signatureAlgOid = X509Certificate.getSignatureAlgOid(); // 获取X509证书签名算法的对象标志符OID(Object Identifier)
    Logger.info(TAG, 'signatureAlgOid: ', signatureAlgOid);
    let signatureAlgParams = X509Certificate.getSignatureAlgParams(); // 获取X509证书签名算法参数
    Logger.info(TAG, 'signatureAlgParams: ', Utils.uint8ArrayToString(signatureAlgParams.data));
    let keyUsage = X509Certificate.getKeyUsage(); // 获取X509证书秘钥用途
    Logger.info(TAG, 'keyUsage: ', Utils.uint8ArrayToString(keyUsage.data));
    let extKeyUsage = X509Certificate.getExtKeyUsage(); //获取X509证书扩展秘钥用途
    Logger.info(TAG, 'extKeyUsage: ', JSON.stringify(extKeyUsage));
    let basicConstraints = X509Certificate.getBasicConstraints(); // 获取X509证书基本约束
    Logger.info(TAG, 'basicConstraints: ', JSON.stringify(basicConstraints));
    let subjectAltNames = X509Certificate.getSubjectAltNames(); // 获取X509证书主体可选名称
    Logger.info(TAG, 'subjectAltNames: ', JSON.stringify(subjectAltNames));
    let issuerAltNames = X509Certificate.getIssuerAltNames(); // 获取X509证书颁发者可选名称
    Logger.info(TAG, 'issuerAltNames: ', JSON.stringify(issuerAltNames));
    let tbs = X509Certificate.getItem(certFramework.CertItemType.CERT_ITEM_TYPE_TBS).data; // 获取X509证书TBS(to be signed)
    Logger.info(TAG, 'tbs: ', base64.fromByteArray(tbs));
    let pubKey = X509Certificate.getItem(certFramework.CertItemType.CERT_ITEM_TYPE_PUBLIC_KEY); // 获取X509证书公钥.
    Logger.info(TAG, 'pubKey: ', base64.fromByteArray(pubKey.data));
    let extensions = X509Certificate.getItem(certFramework.CertItemType.CERT_ITEM_TYPE_EXTENSIONS).data;
    Logger.info(TAG, 'extensions: ', base64.fromByteArray(extensions));
  }
}