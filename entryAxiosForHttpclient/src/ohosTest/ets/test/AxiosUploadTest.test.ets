/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, it, expect } from '@ohos/hypium'
import { axios, AxiosError, AxiosHeaders } from '@ohos/axiosforhttpclient';
import { FormData, AxiosResponse, AxiosProgressEvent, InternalAxiosRequestConfig } from '@ohos/axiosforhttpclient'
import fs from '@ohos.file.fs';
import {
  ConfigModel,
  NormalResultModel,
  PostDataModel,
  DownLoadResultModel,
  UploadResultModel
} from '../../../main/ets/pages/types/types'
import { GlobalContext } from '../testability/GlobalContext';
import hilog from '@ohos.hilog';
import { LOG, XTS_CONFIG } from '../../../main/ets/pages/common/Common'
import { writeFile, readFile } from '../../../main/ets/pages/common/fileFs'

export default function AxiosUploadTest() {

  describe('AxiosUploadTest', () => {
    const BASE_COUNT = 200
    const HTTP_COUNT = 2
    const BASELINE_REQUEST = 2500
    const TAG = LOG.TAG
    const DOMAIN = LOG.DOMAIN
    const config = XTS_CONFIG


    // onUploadProgress
    it('upload_onUploadProgress', 1, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let cacheDir: string = context.cacheDir;

      try {
        writeFile(cacheDir, 'upload_onUploadProgress.txt', '上传文件，查看进度事件')
      }catch (err){
        hilog.info(DOMAIN, TAG, " file error: " +JSON.stringify(err) );
        expect().assertFail()
      }

      let formData = new FormData()
      formData.append('file', 'internal://cache/upload_onUploadProgress.txt');

      let startTime = new Date().getTime()
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
        onUploadProgress: (progressEvent: AxiosProgressEvent): void => {
          if (progressEvent.loaded == progressEvent.total) {
            expect(true).assertTrue()
          }
        }
      }).then((res: AxiosResponse<UploadResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_onUploadProgress averageTime: " + averageTime + ' μs');
        expect(res.data.msg === 'success').assertTrue()
        done()
      })
    })

    // upload by filepath
    it('upload_filepath_selfCreate', 2, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let cacheDir: string = context.cacheDir;

      try {
        writeFile(cacheDir, 'upload_filepath.txt', '上传通过fs创建的文件')
      }catch (err){
        hilog.info(DOMAIN, TAG, " file error: " +JSON.stringify(err) );
        expect().assertFail()
      }

      let formData = new FormData()
      formData.append('file', 'internal://cache/upload_filepath.txt');

      let startTime = new Date().getTime()
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
      }).then((res: AxiosResponse<UploadResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_filepath_selfCreate averageTime: " + averageTime + ' μs');
        expect(res.data.msg === 'success').assertTrue()
        done()
      })
    })

    // upload by arraybuffer
    it('upload_buffer_defaultName', 3, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let cacheDir: string = context.cacheDir;
      let buffer: ArrayBuffer = new ArrayBuffer(1024);
      try {
        // 写入
        writeFile(cacheDir, 'test.txt', '指定上传文件名称')
        // 读取
        let path = cacheDir + '/test.txt';
        buffer = readFile(path)
      } catch (err) {
        hilog.info(DOMAIN, TAG, " upload_buffer_defaultName: " + JSON.stringify(err));
        expect().assertFail()
        done()
      }
      let formData = new FormData()
      formData.append('file', buffer, 'upload_buffer_defaultName.txt');

      let startTime = new Date().getTime()
      // 发送请求
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
        onUploadProgress: (progressEvent: AxiosProgressEvent): void => {
          if (progressEvent.loaded == progressEvent.total) {
            expect(true).assertTrue()
          }
        }
      }).then((res: AxiosResponse<UploadResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_buffer_defaultName averageTime: " + averageTime + ' μs');
        expect(res.data.msg === 'success').assertTrue()
        done()
      }).catch((err: AxiosError) => {
        hilog.info(DOMAIN, TAG, " upload_buffer_defaultName error: " + JSON.stringify(err));
        expect().assertFail()
      })
    })

    // uploading filepath without specifying a file name
    it('upload_filepath', 4, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let cacheDir: string = context.cacheDir;
      let ca: Uint8Array = await context.resourceManager.getRawFileContent("oneWayAuth/ca.pem");

      try {
        writeFile(cacheDir, 'ca.pem', ca.buffer)
      }catch (err){
        hilog.info(DOMAIN, TAG, " upload_filepath error: " +JSON.stringify(err) );
        expect().assertFail()
      }

      let formData = new FormData()
      formData.append('file', 'internal://cache/ca.pem');
      let startTime = new Date().getTime()
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
      }).then((res: AxiosResponse<UploadResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_filepath averageTime: " + averageTime + ' μs');
        expect(res.data.msg === 'success').assertTrue()
        done()
      })
    })

    // uploading arraybuffer type files without specifying a file name
    it('upload_buffer', 5, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let content: Uint8Array = await context.resourceManager.getRawFileContent("oneWayAuth/ca.pem");
      let filesDir: string = context.filesDir;
      let buffer:ArrayBuffer = new ArrayBuffer(1024);

      try {
        writeFile(context.filesDir, 'ca.pem', content.buffer)
        buffer = readFile(filesDir + "/ca.pem")
      } catch (err){
        hilog.info(DOMAIN, TAG, " file error: " +JSON.stringify(err) );
        expect().assertFail()
      }

      // Arraybuffer
      let formData = new FormData()
      formData.append('file', buffer);
      formData.append('name', 'ca.pem');

      let startTime = new Date().getTime()
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
      }).then((res: AxiosResponse<UploadResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_buffer averageTime: " + averageTime + ' μs');
        expect(res.data.msg === 'success').assertTrue()
        done()
      }).catch((err: AxiosError) => {
        hilog.info(DOMAIN, TAG, " upload_buffer error: " + JSON.stringify(err));
        expect().assertFail()
      })
    })

    // uploading arraybuffer type files without specifying a file name
    it('upload_errorUri', 6, async (done: Function) => {
      let context: Context = GlobalContext.getContext().getObject("context") as Context;

      let formData = new FormData()
      formData.append('file', 'internal://cache/upload_errorUri.txt');

      let startTime = new Date().getTime()
      axios.post<UploadResultModel, AxiosResponse<UploadResultModel>, FormData>(config.uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        context: context,
      }).catch((err: AxiosError) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) * 1000 / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " upload_errorUri averageTime: " + averageTime + ' μs');
        expect(Boolean(err.code) === true).assertTrue()
        done()
      })
    })
  })
}
