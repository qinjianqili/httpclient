# node service

一个本地服务，为httpclient提供一些http接口，用于在demo和xts中验证库功能。

## 运行示例

> 安装node

> 安装依赖

```bash
在HttpServe目录下执行 npm i
```

> 运行服务器

```bash

在HttpServe目录下执行 npm run start
```

## 访问主页
在chrome浏览器中输入`http://localhost:4000`访问主页（localhost需要切换服务器ip或者本地电脑ip）。

注意：服务需要和设备在同一局域网内

>接口:

| 接口URL                          | 参数    | 请求方式 |
|--------------------------------| ------------------------ | --------------- |
| http://localhost:4000/tpc/post | id: string | post |

