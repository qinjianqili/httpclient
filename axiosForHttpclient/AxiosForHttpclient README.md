# axiosForHttpclient

## 简介

[Axios](https://github.com/axios/axios) ，是一个基于 promise 的网络请求库，可以运行 node.js 和浏览器中。本库基于[HttpClient](https://gitee.com/openharmony-tpc/httpclient) 原库2.0.0-rc.4版本进行adapter适配，使其可以运行在 OpenHarmony，并沿用axios现有用法和特性。在axios原库的基础上拓展支持了自定义证书，证书锁定，自定义DNS，网络事件监听等。

- http 请求
- Promise API
- 转换 request 和 response 的 data 数据
- 自动转换 JSON data 数据

## 下载安装

```javascript
ohpm install @ohos/axiosforhttpclient
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 需要权限
```
ohos.permission.INTERNET
ohos.permission.GET_NETWORK_INFO
ohos.permission.GET_WIFI_INFO
```

## 使用示例

使用前在demo中entry-->src-->main-->ets-->common-->Common.ets文件中改为正确的服务器地址，同时需要更改demo中的证书相关的配置信息，才可正常的使用demo。

发起一个 GET 请求

axios支持泛型参数，由于ArkTS不再支持any类型，需指定参数的具体类型。
如：axios.get<T = any, R = AxiosResponse<T>, D = any>(url)
- T: 是响应数据类型。当你发送一个 POST 请求时，你可能会发送一个 JSON 对象。T 就是这个 JSON 对象的类型。默认情况下，T 是 any，这意味着你可以发送任何类型的数据。
- R: 是响应体的类型。当服务器返回一个响应时，响应体通常是一个 JSON 对象。R 就是这个 JSON 对象的类型。默认情况下，R 是 AxiosResponse<T>，这意味着响应体是一个 AxiosResponse 对象，它的 data 属性是 T 类型的
- D: 是请求参数的类型。当你发送一个 GET 请求时，你可能会在 URL 中添加一些查询参数。D 就是这些查询参数的类型。参数为空情况下，D 是 null类型。
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
interface UserInfo{
  id: number
  name: string,
  phone: number
}

// 向给定ID的用户发起https请求
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('https://www.xxx.com/user?ID=12345',
  { 
    caPath: 'xxx.pem',
    context: getContext(this),
  })
.then((response: AxiosResponse<UserInfo>)=> {
  // 处理成功情况
  console.info("id" + response.data.id)
  console.info(JSON.stringify(response));
})
.catch((error: AxiosError)=> {
  // 处理错误情况
  console.info(JSON.stringify(error));
})
.then(()=> {
  // 总是会执行
});

// 向给定ID的用户发起http请求
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('http://www.xxx.com/user?ID=12345',{})
.then((response: AxiosResponse<UserInfo>)=> {
    // 处理成功情况
    console.info("id" + response.data.id)
    console.info(JSON.stringify(response));
})
.catch((error: AxiosError)=> {
    // 处理错误情况
    console.info(JSON.stringify(error));
})
.then(()=> {
    // 总是会执行
});

// 上述请求也可以按以下方式完成（可选）
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('http://www.xxx.com/user', {
  params: {
    ID: 12345
  },
})
.then((response:AxiosResponse<UserInfo>) => {
  console.info("id" + response.data.id)
  console.info(JSON.stringify(response));
})
.catch((error:AxiosError) => {
  console.info(JSON.stringify(error));
})
.then(() => {
  // 总是会执行
});

// 上述http请求也可以按以下方式完成（可选）
axios.get<UserInfo, AxiosResponse<UserInfo>, null>('https://www.xxx.com/user', {
  params: {
    ID: 12345
  },
  caPath: 'xxx.pem',
  context: getContext(this),
})
.then((response:AxiosResponse<UserInfo>) => {
    console.info("id" + response.data.id)
    console.info(JSON.stringify(response));
})
.catch((error:AxiosError) => {
    console.info(JSON.stringify(error));
})
.then(() => {
    // 总是会执行
});

// 支持async/await用法
async function getUser() {
  try {
        const response:AxiosResponse = await axios.get<string, AxiosResponse<string>, null>(this.getUrl);
        console.log(JSON.stringify(response));
      } catch (error) {
    console.error(JSON.stringify(error));
  }
}
```

发送一个 POST 请求
```javascript
interface User {
  firstName: string,
  lastName: string
}
   axios.post<string, AxiosResponse<string>, User>('https://www.xxx.com/user', {
     firstName: 'Fred',
     lastName: 'Flintstone'
   },{
      caPath: 'xxx.pem',
      context: getContext(this),
    })
   .then((response: AxiosResponse<string>) => {
     console.info(JSON.stringify(response));
   })
   .catch((error) => {
  console.info(JSON.stringify(error));
});
```

## 使用说明

### axios API

#### 通过向 axios 传递相关配置来创建请求

##### axios(config)
```javascript
// 发送一个get请求
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

axios<string, AxiosResponse<string>, null>({
  method: "get",
  url: 'https://www.xxx.com/info'
  caPath: 'xxx.pem',
  context: getContext(this),
}).then((res: AxiosResponse) => {
  console.info('result:' + JSON.stringify(res.data));
}).catch((error: AxiosError) => {
  console.error(error.message);
})
```

##### axios(url[, config])
```javascript
// 发送一个get请求（默认请求方式）
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

axios.get<string, AxiosResponse<string>, null>('https://www.xxx.com/info', 
{ 
  caPath: 'xxx.pem',
  context: getContext(this),
  params: { key: "value" } 
})
.then((response: AxiosResponse) => {
  console.info("result:" + JSON.stringify(response.data));
})
.catch((error: AxiosError) => {
  console.error("result:" + error.message);
});
```

#### 请求方法的 别名方式 来创建请求
为方便起见，为所有支持的请求方法提供了别名。

- axios.get(url[, config])
- axios.delete(url[, config])
- axios.post(url[, data[, config]])
- axios.put(url[, data[, config]])

> 注意:
在使用别名方法时， 1、url、method、data 这些属性都不必在配置中指定。 2、config中的caPath、context 这些属性必须指定

### 指定返回数据的类型
`responseType` 指定返回数据的类型，默认无此字段。如果设置了此参数，系统将优先返回指定的类型。
选项包括: string:字符串类型; object:对象类型; array_buffer:二进制数组类型。
设置responseType后，response.data中的数据将为指定类型
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

 axios<string, AxiosResponse<string>, null>({
    url: 'https://www.xxx.com/info',
    method: 'get',
    responseType: 'array_buffer', 
    caPath: '', //ca证书路径       // http请求不需要此参数
    context: getContext(this),   // http请求不需要此参数
  }).then((res: AxiosResponse) => {
   // 处理请求成功的逻辑
  })
```

### 自定义ca证书

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath: '', //ca证书路径
    context: getContext(this),
  }).then((res: AxiosResponse) => {
    // 
  }).catch((err: AxiosError) => {
    //
  })
```

### 自定义证书校验

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { X509TrustManager } from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath: '', //ca证书路径
    context: getContext(this),
    sslCertificateManager: new SslCertificateManager(),//证书校验方法
  }).then((res: AxiosResponse) => {
    // 
  }).catch((err: AxiosError) => {
    //
  })

  export class SslCertificateManager implements X509TrustManager{
    checkServerTrusted(X509Certificate: certFramework.X509Cert): void {
       //
     }
    checkClientTrusted(X509Certificate: certFramework.X509Cert): void {
      //
     }
  }
```

### 自定义客户端证书

```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

  axios<InfoModel, AxiosResponse<InfoModel>, null>({
    url: 'https://www.xxx.com/xx',
    method: 'get',
    caPath: '', //ca证书路径
    clientCert: {
        certPath: '', //客户端证书路径
        certType: 'pem', // 客户端证书类型，包括pem、crt、两种
        keyPath: '', //客户端私钥路径
        keyPasswd: '' // 密码
      },
    context: getContext(this),
  }).then((res: AxiosResponse) => {
    
  }).catch((err: AxiosError) => {
    
  })
```

### 设置代理
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

    axios<string, AxiosResponse<string>, null>({
      url: 'http://www.xxx.com',
      method: 'get',
      proxy:{
        host: 'xxx',
        port: xx,
        exclusionList: []
      } 
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
```

### 设置DNS
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { DNS } from '@ohos/httpclient'

    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      dns: new CustomDns(), //自定义DNS方法
      caPath: '', //ca证书路径
      context: getContext(this),
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })

    export class CustomDns implements Dns {
      lookup(hostname: string): Promise<Array<connection.NetAddress>> {
        // todo
      }
    }
```

### 设置优先级
```javascript
import { axios, AxiosError, AxiosResponse, HttpClient} from '@ohos/axiosforhttpclient'

	let client = new HttpClient.Builder();
    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      caPath: '', //ca证书路径
      context: getContext(this),
      client: client,
      priority: 0, //优先级配置
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
```

### 设置事件监听
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
import { EventListener } from '@ohos/axiosforhttpclient';

    axios<string, AxiosResponse<string>, null>({
      url: 'https://www.xxx.com',
      method: 'get',
      eventListener: new EventTest(),//事件监听
      caPath: '', 
      context: getContext(this),
    }).then((res: AxiosResponse) => {
      
    }).catch((err: AxiosError) => {
      
    })
    export class EventTest extends EventListener {
      //
}
```
### 设置优先级队列
```javascript
import { axios, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'

let client = new HttpClient.Builder();
for (let i = 1; i < 100; i++) {
      axios<string, AxiosResponse<string>, null>({
        url: 'http://www.xxx.com?' + 'id=' + i,
        method: 'get',
        client: client,
        priority: i === 50 ? 20 : 1,  // 设置优先级队列,默认最大运行队列并发数是64, 默认最大同host并发数是5, 默认优先级是0，数值越大优先级越高
        async: true   //优先级队列只能在异步请求模式下生效
      }).then((res: AxiosResponse) => {
      
      }).catch((err: AxiosError) => {

      })
    }
```

### 设置缓存
```javascript
import { axios, Cache, AxiosError, AxiosResponse} from '@ohos/axiosforhttpclient'
let context = getContext();
let hereCacheDir: string = context.cacheDir;
let cache: Cache.Cache = new Cache.Cache(hereCacheDir, 10 * 1024 * 1024, context);
axios<string, AxiosResponse<string>, null>({
      url: "https://www.xxx.com",
      method: 'get',
      context: getContext(this),
      caPath: '',
      cache: cache,
      responseType: 'string'
    }).then((res: AxiosResponse<string>) => {

    }).catch((err: AxiosError) => {

})
```

### 设置同步或者异步请求

```javascript
axios<string, AxiosResponse<string>, null>({
  url: 'https://www.xxx.com',
  method: 'get',
  context: getContext(this),
  caPath: 'xxx',
  async: true,   // async不配置或者配置false默认是同步请求，async 配置ture是异步请求
  sslCertificateManager: new SslCertificateManager(),
}).then((res: AxiosResponse<string>) => {
  this.status = res ? res.status : '';
  this.message = res ? JSON.stringify(res.data) : '';
}).catch((err: AxiosError) => {
  this.status = '';
  this.message = err.message;
})
```

### 证书锁定

证书锁定的用法如下：

需要在配置文件中对证书进行相关信息的配置：配置文件路径为：entry/src/main/resources/rawfile/network_config.json

当有该文件存在时，将自动锁定该文件下配置的Hostname与证书公钥Hash，与该配置不符的请求将全部拦截。

配置文件：network_config
```json
{
  "network-security-config": {
    "domain-config": [
      {
        "domains": [
          {
            "include-subdomains": true,
            "name": "x.x.x.x"  // ip地址或域名
          }
        ],
        "pin-set": {
          "expiration": "2024-8-6", //证书锁定的有效期
          "pin": [
            {
              "digest-algorithm": "sha256", //消息摘要的哈希算法，支持格式是sha256 
              "digest": "WAFcHG6pAINrztx343ccddfzLOdfoDS9pPgMv2XHk=" //消息摘要
            }
          ]
        }
      }
    ]
  }
}
```

## 约束与限制

在下述版本验证通过：
4.1 Canary(4.1.3.319), OpenHarmony SDK: API11 (4.1.3.1)

## FAQ

- axiosForHttpclient暂不支持https代理。<br>
  由于该库底层依赖@ohos.net.socket模块，@ohos.net.socket暂未提供https代理的功能。

- axiosForHttpclient暂不支持设置p12格式的证书<br>

  由于该库底层依赖@ohos.net.socket模块，@ohos.net.socket暂未提供设置p12格式证书的功能。

- axiosForHttpclient暂不支持使用系统默认的CA证书<br>由于该库底层依赖@ohos.net.socket模块，@ohos.net.socket在进行网络连通时默认不能系统的CA证书，因此用户在进行https请求时需要手动传入CA证书，即caPath和context不能为空，否则网络请求会提示SSL is null。

## axios原库的用法和说明
具体请参考[Axios](https://gitee.com/openharmony-sig/ohos_axios/blob/master/README.md)

## 贡献代码

使用过程中发现任何问题都可以提[Issue](https://gitee.com/openharmony-tpc/httpclient/issues) 给我们，当然，我们也非常欢迎你给我们提[PR](https://gitee.com/openharmony-tpc/httpclient/pulls) 。

## 开源协议

本项目基于 [MIT](https://gitee.com/openharmony-tpc/httpclient/blob/master/LICENSE) ，请自由地享受和参与开源。