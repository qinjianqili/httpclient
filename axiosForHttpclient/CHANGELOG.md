## 1.0.0-rc.3

1.支持系统默认的CA证书进行通信

## 1.0.0-rc.2

1. 修复通过axios方法返回的headers类型，使返回数据中的headers按照正常格式解析

## 1.0.0-rc.1

1. httpclient适配axios功能,支持axios的上传和下载,支持通过axios配置直接调用httpclient的功能
